`compound.v` : Introducing/describing the use of compound (viz. intersection) types in NL modeling: applications, record, product and pi-types  
`frag.v` : A fragment of NL structure, using intersection types (viz. applications): most categories; canonical structure based selectional restrictions  
`cop.v` : A fragment of NL structure, using intersection types (viz. applications): copulas, who, etc.; experiments; upgraded selectional restrictions; universal supercategories

Written and best viewed in Coq 8.9

FreeBSD License
